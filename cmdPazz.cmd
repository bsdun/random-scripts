@echo off

rem Copyright (c): 2021 Steve
rem License: ISC
rem Creates a file with passwords, or
rem if any argument provided, outputs passwords to console.

SETLOCAL EnableExtensions DisableDelayedExpansion

set "numSymbols=30"
set /P "userInput=Enter a number from 4-127 range: "
set /A "userInput=%userInput:~0,3%"
if %userInput% GEQ 4 if %userInput% LEQ 127 set "numSymbols=%userInput%"
set "numLines=100"
set "passSequence="
set "passString="
set "rndSource="
set "tempValue="

if not "%1"=="" (
  set "outFile="
  goto :noOutFile
)
set "outFile=>>%~dp0pazz.txt"
if exist "%outFile:~2%" del "%outFile:~2%"
:noOutFile

set "lsource1=#$%%&()*+-./:;=>?@[]_{|}!"
set "lsource2=1234567890"
set "lsource3=ABCDEFGHIJKLMNOPQRSTUVWXYZ"
set "lsource4=abcdefghijklmnopqrstuvwxyz"

set "lsize1=24"
set "lsize2=10"
set "lsize3=26"
set "lsize4=26"

set "lcount1=0"
set "lcount2=0"
set "lcount3=0"
set "lcount4=0"


SETLOCAL EnableExtensions EnableDelayedExpansion

for /L %%A in (1, 1, %numLines%) do (
  set "passSequence="
  set "passString="
  call :genPass
)
goto :EOF

:genPass
  for /L %%B in (1, 1, %numSymbols%) do (
    set /A "rndSource=(!RANDOM! %% 4) + 1"
    set "passSequence=!passSequence!!rndSource! "
    set /A "lcount!rndSource! += 1"
  )

  :fixLoop
    set /A "tempValue=lcount1 * lcount2 * lcount3 * lcount4"
    if %tempValue% NEQ 0 goto :okString
    set /A "lcount%passSequence:~0,1% -= 1"
    set "passSequence=%passSequence:~2%
    set /A "rndSource=(%RANDOM% %% 4) + 1"
    set "passSequence=%passSequence%%rndSource% "
    set /A "lcount%rndSource% += 1"
    goto :fixLoop

  :okString
    for %%C in (%passSequence%) do (
      set /A "rndSource=!RANDOM! %% lsize%%C"
      for %%D in (!rndSource!) do set "passString=!passString!!lsource%%C:~%%D,1!"
    )
    %outFile% echo !passString!
    goto :EOF

